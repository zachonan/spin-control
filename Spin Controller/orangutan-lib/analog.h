// analog.h
//
// Tom Benedict

#ifndef _ANALOG_
#define _ANALOG_

// Initialize the ADC to do 10-bit conversions:
void analog_init(void);

// Read out the specified analog channel to 10 bits
uint16_t analog10(uint8_t channel);

// Read out the specified analog channel to 8 bits
uint8_t analog8(uint8_t channel);

#endif //_ANALOG_
