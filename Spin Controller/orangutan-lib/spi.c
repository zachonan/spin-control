// spi.c
//
// SPI master routines were pulled from the Atmel ATMega168 datasheet.

#include "device.h"

#include <avr/io.h>
#include <util/delay.h>

#include "spi.h"

// Initialize the SPI as master
void spi_init()
{
	// make the MOSI, SCK, and SS pins outputs
	SPI_DDR |= ( 1 << SPI_MOSI ) | ( 1 << SPI_SCK ) | ( 1<< SPI_SS );

	// make sure the MISO pin is input
	SPI_DDR &= ~( 1 << SPI_MISO );

	// set up the SPI module: SPI enabled, MSB first, master mode,
	//  clock polarity and phase = 0, F_osc/8
	SPI_SPCR = ( 1 << SPI_SPE ) | ( 1 << SPI_MSTR ) | ( 1 << SPI_SPR0 );
//	SPI_SPSR = 1;     // set double SPI speed for F_osc/8
}

// Transfer a byte of data 
uint8_t spi_transfer( uint8_t data )
{
	// Start transmission
	SPI_SPDR = data;

	// Wait for the transmission to complete
	spi_wait();

	return SPI_SPDR;
}
